
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
    id int NOT NULL AUTO_INCREMENT,
    email varchar(300) NOT NULL,
    password varchar(300) NOT NULL,
    name varchar(500) NOT NULL,
    student_class varchar(50),
    generation int,
    yob date,
    address varchar(500),
    job varchar(500),
    company varchar(500),
    position varchar(500),
    phone_number varchar(500),
    note varchar(500),
    role_id int,
	created datetime,
	updated datetime,
	deleted datetime,
    PRIMARY KEY (`id`),
    UNIQUE KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- add table opportunites and user_opportunities 2019/12/10
DROP TABLE IF EXISTS `opportunities`;
CREATE TABLE `opportunities` (
    id int NOT NULL AUTO_INCREMENT,
    create_user_id int NOT NULL,
    title varchar(300) NOT NULL,
    content varchar(100000) NOT NULL,
    date_from datetime,
    date_to datetime,
    status int,
	created datetime,
	updated datetime,
	deleted datetime,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `opportunity_users`;
CREATE TABLE `opportunity_users` (
    id int NOT NULL AUTO_INCREMENT,
    interested_user_id int NOT NULL,
    opportunity_id varchar(300) NOT NULL,
    status int NOT NULL,
	created datetime,
	updated datetime,
	deleted datetime,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
