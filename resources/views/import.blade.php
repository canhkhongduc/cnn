@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container">
                    <form method="post" action="{{ url('import/import_users') }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-4 custom-file">
                                <input type="file" class="custom-file-input" id="users_header_file" name="users_header_file" required>
                                <label class="custom-file-label" for="users_header_file" data-browse="Users"></label>
                                <div class="invalid-feedback">Example invalid custom file feedback</div>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary ml-1">Import</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="container">
                    <form method="post" action="{{ url('import/import_users_2') }}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-sm-4 custom-file">
                                <input type="file" class="custom-file-input" id="users_header_file" name="users_header_file" required>
                                <label class="custom-file-label" for="users_header_file" data-browse="Users"></label>
                                <div class="invalid-feedback">Example invalid custom file feedback</div>
                            </div>
                            <div class="col-sm-3">
                                <button type="submit" class="btn btn-primary ml-1">Import</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
