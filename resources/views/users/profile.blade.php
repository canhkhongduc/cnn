@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
           
        <div class="col-md-8">
            
            <div class="card">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="card-header">Thông tin tài khoản</div>

                <form method="post" action="{{url('users/update')}}">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Tên *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus {{ $editable ? '' : 'readonly' }}>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
        
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email *</label>

                            <div class="col-md-6">
                                @if($editable)
                                <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required autocomplete="email">
                                @else
                                <a href = "mailto: {{$user['email']}}">
                                    <input id="email" type="email" class="form-control contact-input" name="email" value="{{ $user->email }}" required autocomplete="email" readonly>
                                </a>
                                @endif
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row" style="margin: auto;">
                            <div class="col">
                                <label for="student-class" class="col-md-4 col-form-label text-md-right">Lớp</label>
                                <input id="student-class" type="text" class="form-control" name="student_class" autocomplete="new-password" placeholder="Lớp" value="{{ $user->student_class }}" {{ $editable ? '' : 'readonly' }}>
                            </div>
                            <div class="col">
                                <label for="generation" class="col-md-4 col-form-label text-md-right">Khóa</label>
                                <input id="generation" type="text" class="form-control" name="generation" autocomplete="new-password" placeholder="Khóa" value="{{ $user->generation }}" {{ $editable ? '' : 'readonly' }}>
                            </div>
                            <div class="col">
                                <label for="yob" class="col-md-4 col-form-label text-md-right">YOB</label>
                                <input id="yob" type="number" class="form-control" name="yob" autocomplete="new-password" placeholder="YOB" value="{{ $user->yob }}" {{ $editable ? '' : 'readonly' }}>
                            </div>
                        </div>
                        <br/>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Nơi cư trú (Thành phố - quốc gia)</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" autocomplete="new-password" value="{{ $user->address }}" {{ $editable ? '' : 'readonly' }}>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job" class="col-md-4 col-form-label text-md-right">Ngành nghề công tác/Lĩnh vực kinh doanh</label>

                            <div class="col-md-6">
                                <textarea id="job" type="text" class="form-control" name="job" autocomplete="new-password" {{ $editable ? '' : 'readonly' }}>{{ $user->job }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company" class="col-md-4 col-form-label text-md-right">Đơn vị công tác/Tên doanh nghiệp</label>

                            <div class="col-md-6">
                                <textarea id="company" type="text" class="form-control" name="company" autocomplete="new-password" {{ $editable ? '' : 'readonly' }}>{{ $user->company }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="website" class="col-md-4 col-form-label text-md-right">Website/Fanpage kinh doanh</label>

                            <div class="col-md-6">
                                <textarea id="website" type="text" class="form-control" name="website" autocomplete="new-password" {{ $editable ? '' : 'readonly' }}>{{ $user->website }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="position" class="col-md-4 col-form-label text-md-right">Chức vụ</label>

                            <div class="col-md-6">
                                <textarea id="position" type="text" class="form-control" name="position" autocomplete="new-password" value="" {{ $editable ? '' : 'readonly' }}>{{ $user->position }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phonenumber" class="col-md-4 col-form-label text-md-right">Số điện thoại</label>

                            <div class="col-md-6">
                                @if(!empty($user['phone_number']) && !$editable)
                                <a href="tel:{{$user['phone_number']}}"><input id="phonenumber" type="text" class="form-control contact-input" name="phone_number" autocomplete="new-password" value="{{ $user->phone_number }}" {{ $editable ? '' : 'readonly' }}/></a>
                                @else
                                <input id="phonenumber" type="text" class="form-control" name="phone_number" autocomplete="new-password" value="{{ $user->phone_number }}" {{ $editable ? '' : 'readonly' }}/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="facebook" class="col-md-4 col-form-label text-md-right">Facebook cá nhân</label>

                            <div class="col-md-6">
                                @if(!empty($user['facebook']) && !$editable)
                                <a href="https://www.facebook.com/search/top/?q={{ $user['facebook'] }}&epa=SEARCH_BOX" target="_blank"><input id="facebook" type="text" class="form-control contact-input" name="facebook" autocomplete="new-password" value="{{ $user->facebook }}" {{ $editable ? '' : 'readonly' }}/></a>
                                @else
                                <input id="facebook" type="text" class="form-control" name="facebook" autocomplete="new-password" value="{{ $user->facebook }}" {{ $editable ? '' : 'readonly' }}/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="experience" class="col-md-4 col-form-label text-md-right">Khả năng, kinh nghiệm</label>

                            <div class="col-md-6">
                                <textarea id="experience" type="text" class="form-control" name="experience" autocomplete="new-password" {{ $editable ? '' : 'readonly' }}>{{ $user->experience }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="note" class="col-md-4 col-form-label text-md-right">Ghi chú, nhu cầu</label>

                            <div class="col-md-6">
                                <textarea id="note" type="text" class="form-control" name="note" autocomplete="new-password" {{ $editable ? '' : 'readonly' }}>{{ $user->note }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div>
                            @if($editable)
                            <button type="submit" name="" class="btn btn-primary">Lưu</button>
                            @else
                            <button type="button" name="" class="btn btn-primary" onclick="window.location = '{{ url('opportunities?email=' . $user->email) }}'">Cơ hội hợp tác</button>
                            <button type="button" name="" class="btn btn-secondary" onclick="window.history.back();">Quay lại</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
