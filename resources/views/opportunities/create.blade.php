@extends('layouts.app')

@section('content')
<div id="main-opportunity-create">
    <div class="container">
        <div class="row justify-content-center">
            
            <div class="col-md-10">
                
                <div class="card">
                    <div class="card-header">Đăng cơ hội hợp tác</div>

                    <form method="post" action="{{url('opportunities/create')}}" id="create-opportunity-form">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Tiêu đề *</label>
                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" required autocomplete="title" autofocus value="{{ empty(old('title')) ? "" : old('title')}}"/>

                                    <div class="text-danger">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date_from" class="col-md-4 col-form-label text-md-right">Thời gian</label>
                                <div class="col-md-6">
                                    <input type="date" id="date_from" name="date_from" class="form-control mr-2 datepick bg-white" placeholder="Từ..." value="{{ empty(old('date_from')) ? "" : old('date_from')}}"/>
                                    <div class="text-danger">
                                        {{ $errors->first('date_from') }}
                                    </div>~
                                    <input type="date" id="date_to" name="date_to" class="form-control mr-2 datepick bg-white" placeholder="Đến..." value="{{ empty(old('date_from')) ? "" : old('date_to')}}"/>
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 70px;">
                                <label for="content" class="col-md-4 col-form-label text-md-right">Nội dung</label>
                                <div class="col-md-6">
                                    <input name="content" type="hidden" {{ $errors->has('content') ? ' is-invalid' : '' }}>
                                    <div id="editor">
                                    </div>
                                    <div class="text-danger">
                                        {{ $errors->first('content') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div>

                                <button type="submit" name="" class="btn btn-primary">Lưu</button>
                                <button type="button" name="" class="btn btn-secondary" onclick="window.location='{{ url("opportunities") }}'">Quay lại</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
