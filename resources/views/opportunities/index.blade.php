@extends('layouts.app')

@section('content')
<div id="main-opportunity-index">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 class="h5">Cơ hội hợp tác</h1>
                </div>
                <div class="card bg-light mb-3">
                    <div class="card-body">
                        <form method="post" action="{{url('opportunities')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-auto form-inline">
                                    <label for="search_word" class="form-label-sm mr-2">Tìm kiếm</label>
                                    <input type="text" class="form-control form-control-sm mr-2" placeholder="Tên dự án, người đăng, nội dung..." style="width: 250px" id="search_word" name="search_word" value="{{$search_word}}">
                                </div>
                                <div class="col-auto form-inline mb-2">
                                    <label for="date" class="form-label-sm mr-2">Thời gian</label>
                                    <input type="date" id="date_from" name="date_from" class="form-control form-control-sm mr-2 datepick bg-white" placeholder="Từ" value="{{$date_from}}"/>
                                    <input type="date" id="date_to" name="date_to" class="form-control form-control-sm mx-2 datepick bg-white" placeholder="Đến" value="{{$date_to}}"/>
                                </div>
                                <div class="col-auto">
                                    <button type="submit" class="btn btn-sm btn-dark" name="submitted">Tìm kiếm</button>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="col-auto">
                                    <button type="button" class="btn btn-primary" onclick="location.href='{{ url('opportunities/create') }}'">Đăng dự án</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <input type="hidden" id="userId" name="userId" value="{{ Auth::user()->id }}">
                @foreach ($opportunities as $index => $opportunity)
                    <div class="card opp-card">
                        <div class="card-body">
                        <h5 class="card-title">{{ $opportunity['title'] }}</h5>
                        <input type="hidden" name="content" value="{{ $opportunity['content'] }}" id="content-{{$index}}">
                        
                        <a href="{{ url('users/profile?email=' . $opportunity['create_user']['email'])  }}">{{ $opportunity['create_user']['name'] }}</a>
                        <h6 class="card-subtitle mb-2 text-muted opp-date">{{dfdate('Y/m/d', $opportunity, 'date_from')}} ~ {{dfdate('Y/m/d', $opportunity, 'date_to')}}</h6>
                        <div id="disable-editor-{{$index}}" class="card-text">
                        </div>
                        
                        @if($current_user_id == $opportunity['create_user_id'])
                            <a href="#" id="btn-list-interested" class="card-link" data-toggle="modal" data-target="#modal-interested-users" data-oid="{{ $opportunity['id'] }}"><b>Đã quan tâm ({{ count($opportunity['interested_users']) }} người)</b></a>
                            <a href="{{ url('/opportunities/update?id=' . $opportunity['id']) }}" class="card-link">Sửa</a>
                            <a id="" href="#" data-toggle="modal" data-target="#deleteModal" data-oid="{{ $opportunity['id'] }}" class="card-link">Xóa</a>
                        @else
                            @if(!$opportunity['interested'])
                            <a id="btn-interest-{{$opportunity['id']}}" class="card-link btn-interest">Quan tâm</a>
                            <a id="btn-disinterest-{{$opportunity['id']}}" class="card-link btn-disinterest" style="display:none;">Bỏ quan tâm</a>
                            @else
                            <a id="btn-interest-{{$opportunity['id']}}" class="card-link btn-interest" style="display:none;">Quan tâm</a>
                            <a id="btn-disinterest-{{$opportunity['id']}}" class="card-link btn-disinterest">Bỏ quan tâm</a>
                            @endif
                            <a href="{{ url('users/profile?email=' . $opportunity['create_user']['email'])  }}" class="card-link">Liên hệ</a>
                        @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- Modal delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Xác nhận</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Bạn có chắc chắn muốn xóa dự án này?</h5>
      </div>
      <div class="modal-footer">
        <form id="userForm" action="{{url('opportunities/delete')}}" method="post">
            @csrf
            <input type="hidden" name="id" id="opportunity-id">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            <button type="submit" class="btn btn-danger" id="btn-delete-opportunity">Tiếp tục</button>
        </form>
       
      </div>
    </div>
  </div>
</div>

<!-- Modal interested users-->
<div class="modal fade modal-interested-users" id="modal-interested-users" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Danh sách người quan tâm</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-interested-users-body" id="modal-interested-users-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection
