@extends('layouts.app')

@section('content')
<div id="main-opportunity-update">
    <div class="container">
        <div class="row justify-content-center">
            
            <div class="col-md-10">
                
                <div class="card">
                    <div class="card-header">Sửa cơ hội hợp tác</div>

                    <form method="post" action="{{url('opportunities/update')}}" id="update-opportunity-form">
                        @csrf
                        <input name="id" type="hidden" value="{{ $opportunity['id']}}" />
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Tiêu đề *</label>
                                <div class="col-md-6">
                                    <input id="title" type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" required autocomplete="title" autofocus value="{{ $opportunity['title'] }}"/>

                                    <div class="text-danger">
                                        {{ $errors->first('title') }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="date_from" class="col-md-4 col-form-label text-md-right">Thời gian</label>
                                <div class="col-md-6">
                                    <input type="text" id="date_from" name="date_from" class="form-control form-control-sm mr-2 datepick bg-white" placeholder="Ex：2012/05/31" value="{{ $opportunity['date_from'] }}"/>
                                    <div class="text-danger">
                                        {{ $errors->first('date_from') }}
                                    </div>~
                                    <input type="text" id="date_to" name="date_to" class="form-control form-control-sm mr-2 datepick bg-white" placeholder="Ex：2012/05/31" value="{{ $opportunity['date_to'] }}" />
                                </div>
                            </div>
                            <div class="form-group row" style="margin-bottom: 40px;">
                                <label for="content" class="col-md-4 col-form-label text-md-right">Nội dung</label>
                                <div class="col-md-6">
                                    <input name="content" type="hidden" value="{{ $opportunity['content']}}">
                                    <div id="update-editor">
                                    </div>
                                    <div class="text-danger">
                                        {{ $errors->first('content') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div>

                                <button type="submit" name="" class="btn btn-primary">Lưu</button>
                                <button type="button" name="" class="btn btn-secondary" onclick="window.location='{{ url("opportunities") }}'">Quay lại</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
