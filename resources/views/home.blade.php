@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <table id="tb-users" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tên</th>
                                    <th>Lớp</th>
                                    <th>Khóa</th>
                                    <th>YOB</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Nơi cư trú</th>
                                    <th>Ngành nghề công tác</th>
                                    <th>Đơn vị công tác</th>
                                    <th>Website/Fanpage</th>
                                    <th>Chức vụ</th>
                                    <th>FB</th>
                                    <th>Kinh nghiệm</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr data-id="{{$user['id']}}">
                                    <td><a href="{{ url('users/profile?email=' . $user["email"])  }}">{{ $user['name'] }}</a></td>
                                    <td>{{ $user['student_class'] }}</td>
                                    <td>{{ $user['generation'] }}</td>
                                    <td>{{ $user['yob'] }}</td>
                                    <td>
                                        <a href = "mailto: {{$user['email']}}">
                                            {{ $user['email'] }}
                                        </a>
                                    </td>
                                    <td>
                                        @if(!empty($user['facebook']))
                                        <a href="tel:{{$user['phone_number']}}">
                                            {{ $user['phone_number'] }}
                                        </a>
                                        @endif
                                    </td>
                                    <td>{{ $user['address'] }}</td>
                                    <td>{{ $user['job'] }}</td>
                                    <td>{{ $user['company'] }}</td>
                                    <td>{{ $user['website'] }}</td>
                                    <td>{{ $user['position'] }}</td>
                                    <td>
                                        @if(!empty($user['facebook']))
                                        <a href="https://www.facebook.com/search/top/?q={{ $user['facebook'] }}&epa=SEARCH_BOX">
                                            {{ $user['facebook'] }}
                                        </a>
                                        @endif
                                    </td>
                                    <td>{{ $user['experience'] }}</td>
                                    <td>{{ $user['note'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
