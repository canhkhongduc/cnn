@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Đăng ký</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <h5 class="card-title">Tài khoản</h5>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Tên *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email *</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Mật khẩu *</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="new-password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Xác nhận mật khẩu *</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <h5 class="card-title">Thông tin cá nhân</h5>
                        <div class="form-group row" style="margin: auto;">
                            <div class="col">
                                <input id="student-class" type="text" class="form-control" name="student_class" autocomplete="new-password" placeholder="Lớp">
                            </div>
                            <div class="col">
                                <input id="generation" type="text" class="form-control" name="generation" autocomplete="new-password" placeholder="Khóa">
                            </div>
                            <div class="col">
                                <input id="yob" type="number" class="form-control" name="yob" autocomplete="new-password" placeholder="YOB">
                            </div>
                        </div>
                        <br/>
                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">Nơi cư trú (Thành phố - quốc gia)</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="job" class="col-md-4 col-form-label text-md-right">Ngành nghề công tác/Lĩnh vực kinh doanh</label>

                            <div class="col-md-6">
                                <input id="job" type="text" class="form-control" name="job" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="company" class="col-md-4 col-form-label text-md-right">Đơn vị công tác/Tên doanh nghiệp</label>

                            <div class="col-md-6">
                                <input id="company" type="text" class="form-control" name="company" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="website" class="col-md-4 col-form-label text-md-right">Website/Fanpage kinh doanh</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="position" class="col-md-4 col-form-label text-md-right">Chức vụ</label>

                            <div class="col-md-6">
                                <input id="position" type="text" class="form-control" name="position" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phonenumber" class="col-md-4 col-form-label text-md-right">Số điện thoại</label>

                            <div class="col-md-6">
                                <input id="phonenumber" type="text" class="form-control" name="phone_number" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="facebook" class="col-md-4 col-form-label text-md-right">Facebook cá nhân</label>

                            <div class="col-md-6">
                                <input id="facebook" type="text" class="form-control" name="facebook" autocomplete="new-password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="experience" class="col-md-4 col-form-label text-md-right">Khả năng, kinh nghiệm</label>

                            <div class="col-md-6">
                                <textarea id="experience" type="text" class="form-control" name="experience" autocomplete="new-password"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="note" class="col-md-4 col-form-label text-md-right">Ghi chú, nhu cầu</label>

                            <div class="col-md-6">
                                <textarea id="note" type="text" class="form-control" name="note" autocomplete="new-password"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Đăng ký
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
