$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

const dataTableLang = {
	"sEmptyTable":     "Không có data",
	"sInfo":           " Hiển thị từ _START_ đến _END_ của _TOTAL_ records",
	"sInfoEmpty":      " Hiển thị từ 0 đến 0 của 0 record",
	"sInfoFiltered":   "（Toàn bộ có _MAX_ records）",
	"sInfoPostFix":    "",
	"sInfoThousands":  ",",
	"sLengthMenu":     "_MENU_ records",
	"sLoadingRecords": "Loading...",
	"sProcessing":     '<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="color:#999; opacity:0.7;"></i><span class="sr-only">Loading...</span>',
	"sSearch":         "Tìm kiếm:",
	"sZeroRecords":    "Không có record nào",
	"oPaginate": {
		"sFirst":    "Đầu",
		"sLast":     "Cuối",
		"sNext":     "Sau",
		"sPrevious": "Trước"
	},
	"oAria": {
		"sSortAscending":  ": Sắp xếp cột theo thứ tự tăng dần",
		"sSortDescending": ": Sắp xếp cột theo thứ tự giảm dần"
	}
};

$(window).on('load', function(){
    $("#tb-users").DataTable({
        ordering: true,
        info: true,
        paging: true,
        displayLength: 10,
        stateSave: true,
		language: dataTableLang,
		scrollX: true,
		bAutoWidth: true,
		columnDefs: [
		{ 
			width: "160px", 
			targets: [7, 8, 12, 13]
		}
		]
	});
	flatpickr("input.datepick", {
		altInput: true,
		altFormat: "Y/m/d",
		dateFormat: "Y-m-d",
		locale: "vn"
	});
	let disableEditors = document.querySelectorAll('[id^=disable-editor]');
	for(let i = 0; i < disableEditors.length; i++) {
		disableEditors[i].style.border = "none";
		let contentId = "content-" + i;
		let oppContent = document.getElementById(contentId);
		let quillId = "#" + disableEditors[i].id;
		let disableQuill = new Quill(quillId, {
		theme: 'snow'
		});
		disableQuill.setContents(JSON.parse(oppContent.value));
		disableQuill.disable();
	}
  	$('#main-opportunity-index .ql-toolbar').css('display', 'none');
	if(document.getElementById('editor')) {
		let quill = new Quill('#editor', {
		theme: 'snow',
		placeholder: 'Điền nội dung hợp tác...'
		});
		var form = document.getElementById('create-opportunity-form');
		form.onsubmit = function () {
			// Populate hidden form on submit
			var content = document.querySelector('input[name=content]');
			content.value = JSON.stringify(quill.getContents());
			return true;
		};
	}
  	if(document.getElementById('update-editor')) {
		let updateQuill = new Quill('#update-editor', {
			theme: 'snow',
			placeholder: 'Điền nội dung hợp tác...'
		});
		let content = document.querySelector('input[name=content]');
		updateQuill.setContents(JSON.parse(content.value));
		var form = document.getElementById('update-opportunity-form');
		form.onsubmit = function () {
			// Populate hidden form on submit
			var content = document.querySelector('input[name=content]');
			content.value = JSON.stringify(updateQuill.getContents());
			return true;
		};
	}
	$('.btn-interest').on('click', function(e){
		let oppId = e.currentTarget.id.replace("btn-interest-", "");
		let userId = $('#userId').val();
		$.ajax({
			url: baseurl+'/opportunities/interest',
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				interested_user_id: userId,
				opportunity_id: oppId
			},
			success: function(json){
				let opportunity_id = json.opportunity_id;
				let interestBtnId = "#btn-interest-" + opportunity_id;
				let disinterestBtnId = "#btn-disinterest-" + opportunity_id;
				$(interestBtnId).css('display','none');
				$(disinterestBtnId).css('display','block');
			},
			error: function(xhr, ts, err){
				console.log('api error..');
			}
		});
	});

	$('.btn-disinterest').on('click', function(e){
		let oppId = e.currentTarget.id.replace("btn-disinterest-", "");
		let userId = $('#userId').val();
		$.ajax({
			url: baseurl+'/opportunities/disinterest',
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				interested_user_id: userId,
				opportunity_id: oppId
			},
			success: function(json){
				let opportunity_id = json.opportunity_id;
				let interestBtnId = "#btn-interest-" + opportunity_id;
				let disinterestBtnId = "#btn-disinterest-" + opportunity_id;
				$(interestBtnId).css('display','block');
				$(disinterestBtnId).css('display','none');
			},
			error: function(xhr, ts, err){
				console.log('api error..');
			}
		});
	});

	// $("#btn-delete-opportunity").click(function(e) {
	// 	let anchorDelete = document.getElementById('anchor-delete-opportunity');
	// 	let oppId = anchorDelete.getAttribute('data-oid');
	// 	location.href = baseurl + "/opportunities/delete?id=" + oppId;
	// });
	$('#deleteModal').on('shown.bs.modal', function (e) {
		let anchorInterested = e.relatedTarget;
		let oppId = anchorInterested.getAttribute('data-oid');
		$('#opportunity-id').val(oppId);
	});
	$('#modal-interested-users').on('shown.bs.modal', function (e) {
		let anchorInterested = e.relatedTarget;
		let oppId = anchorInterested.getAttribute('data-oid');
		$.ajax({
			url: baseurl+'/opportunities/listInterested',
			type: "POST",
			cache: false,
			dataType: "json",
			data: {
				opportunity_id: oppId
			},
			success: function(users){
				if(users.length == 0) {
					$('#modal-interested-users-body').append("<h5>Chưa có người quan tâm.</h5>");
				}
				for(let i = 0; i < users.length; i++) {
					let appendHtml = '<h5><a href="' + baseurl + "/users/profile?email=" + users[i]['user']['email'] + '">' + users[i]['user']['name'] + '</a></h5><hr>'
					$('#modal-interested-users-body').append(appendHtml);
				}
			},
			error: function(xhr, ts, err){
				console.log('api error..');
			}
		});
	});
	$('#modal-interested-users').on('hidden.bs.modal', function () {
		$('#modal-interested-users-body').html("");
	});
});
function doPost(url, params){
	console_log(url);
	console_log(params);
	var form = document.createElement("form");
	document.body.appendChild(form);
	for (i in params) {
		var hidden = document.createElement("input");
		hidden.setAttribute("type", "hidden");
		hidden.setAttribute("name", params[i].key);
		hidden.setAttribute("value", params[i].value);
		form.appendChild(hidden);
	}

	// CSRF Token
	var hidden = document.createElement("input");
	hidden.setAttribute("type", "hidden");
	hidden.setAttribute("name", "_token");
	hidden.setAttribute("value", $('meta[name="csrf-token"]').attr('content'));
	form.appendChild(hidden);

	form.action = url;
	form.method = "post";
	form.submit();
}