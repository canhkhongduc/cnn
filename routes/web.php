<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('import', 'ImportController@index');
Route::match(['get', 'post'], 'import/import_users', 'ImportController@import_users');
Route::match(['get', 'post'], 'import/import_users_2', 'ImportController@import_users_2');
Route::match(['get', 'post'], 'users/changePassword', 'UserController@changePassword');
Route::match(['get', 'post'], 'users/updatePassword', 'UserController@updatePassword');
Route::match(['get', 'post'], 'users/profile', 'UserController@profile');
Route::match(['get', 'post'], 'users/update', 'UserController@update');
Route::match(['get', 'post'], 'opportunities', 'OpportunityController@index');
Route::match(['get', 'post'], 'opportunities/index', 'OpportunityController@index');
Route::match(['get'], 'opportunities/create', 'OpportunityController@showCreateForm');
Route::match(['post'], 'opportunities/create', 'OpportunityController@save');
Route::match(['get'], 'opportunities/update', 'OpportunityController@showUpdateForm');
Route::match(['post'], 'opportunities/update', 'OpportunityController@update');
Route::match(['post'], 'opportunities/interest', 'OpportunityController@interest');
Route::match(['post'], 'opportunities/disinterest', 'OpportunityController@disinterest');
Route::match(['get'], 'opportunities/interested', 'OpportunityController@interested');
Route::match(['post'], 'opportunities/delete', 'OpportunityController@delete');
Route::match(['post'], 'opportunities/listInterested', 'OpportunityController@listInterested');
