A web application to connect high school alumnus in a network.
Members can view others' contact, post opportunities and be interested in others' opportunities.

## Available Scripts

### `git clone https://gitlab.com/canhkhongduc/cnn`

In the project directory, you can run:
### `npm install`
### `composer install`
### `php artisan serve`

Runs the app in the development mode.<br>
Open [http://localhost:8000](http://localhost:8000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.
### `npm run build`

Builds the app for production to the `public` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your application is ready to be deployed!