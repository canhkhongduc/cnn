<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public $incrementing = false;

    public $columns = array();

    public function allIndexed($id=null) {
        if (empty($id) && !is_array($this->primaryKey)) {
            $id = $this->primaryKey;
        } else {
            $id = 'id';
        }
        $arr = self::all()->toArray();
        $ret = array();
        foreach ($arr as $e) {
            $ret[$e[$id]] = $e;
        }
        return $ret;
    }

    public function insertPart($data) {
        \Log::debug('insertPart');
        $part = array_only($data, $this->columns);
        \Log::debug(print_r($part, true));
        if (empty($part)) return;
        \DB::enableQueryLog();
        self::insert($part);
        \Log::debug(\DB::getQueryLog());
    }

    public function insertPartGetId($data) {
        \Log::debug('insertPartGetId');
        $part = array_only($data, $this->columns);
        \Log::debug(print_r($part, true));
        if (empty($part)) return null;
        \DB::enableQueryLog();
        $ret = self::insertGetId($part);
        \Log::debug(\DB::getQueryLog());
        return $ret;
    }

    public function updatePart($data, $where) {
        \Log::debug('updatePart');
        $part = array_only($data, $this->columns);
        \Log::debug(print_r($part, true));
        if (empty($part)) return;
        $hasData = false;
        foreach ($part as $key => $val) {
            if ($key == $this->primaryKey) continue;
            if (in_array($key, array(
                'created_at', 'updated_at', 'deleted_at',
                'created_user_id', 'updated_user_id', 'deleted_id_id',
            ))) continue;
            $hasData = true;
            break;
        }
        if (!$hasData) return;
        \DB::enableQueryLog();
        self::where($where)->update($part);
        \Log::debug(\DB::getQueryLog());
    }

    public function insertOrUpdatePart($data, $where) {
        \Log::debug('insertOrUpdatePart');
        if (count(self::where($where)->get()) < 1) {
            $this->insertPart(array_merge($data, $where));
        } else {
            $this->updatePart($data, $where);
        }
    }

}
