<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class OpportunityUser extends BaseModel
{
    use SoftDeletes;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    const DELETED_AT = 'deleted';
    protected $dates = ['deleted'];

    public function user()
    {
        return $this->belongsTo('App\User', 'foreign_key');
    }

    public function opportunity()
    {
        return $this->belongsTo('App\Models\Opportunity', 'foreign_key');
    }
}
