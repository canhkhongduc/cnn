<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Opportunity extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'date_from', 'date_to', 'status', 'deleted'
    ];
    use SoftDeletes;

    const CREATED_AT = 'created';
    const UPDATED_AT = 'updated';
    const DELETED_AT = 'deleted';
    protected $dates = ['deleted'];

}
