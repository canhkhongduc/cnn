<?php

if (! function_exists('log_array')) {
    function log_array($arr, $caption='')
    {
        if (!empty($caption)) $caption .= '=';
        \Log::debug($caption . print_r($arr, true));
    }
}

if (! function_exists('toint')) {
    function toint($val)
    {
        if (empty($val) && $val !== 0) return null;
        $val = str_replace(',','',$val);
        return intval($val);
    }
}

if (! function_exists('dfnull')) {
    function dfnull($val)
    {
        if (empty($val) && $val !== 0) return null;
        return $val;
    }
}

if (! function_exists('df')) {
    function df($arr, $key, $df='')
    {
        if (!isset($arr[$key])) return $df;
        if (empty($arr[$key]) && $arr[$key] !== 0) return $df;
        return $arr[$key];
    }
}

if (! function_exists('dfnum')) {
    function dfnum($arr, $key, $df='')
    {
        if (!isset($arr[$key])) return $df;
        if (empty($arr[$key]) && $arr[$key] !== 0) return $df;
        return number_format(htmlspecialchars($arr[$key]));
    }
}

if (! function_exists('dfdate')) {
    function dfdate($fmt, $arr, $key, $df='')
    {
        if (!isset($arr[$key])) return $df;
        if (empty($arr[$key])) return $df;
        $timestamp = htmlspecialchars($arr[$key]);
		$t = $timestamp instanceof DateTime ? $timestamp : new DateTime($timestamp);
		return $t->format($fmt);
    }
}

if (! function_exists('addemptyopt')) {
    function addemptyopt($arr)
    {
        $ret = array(''=>'');
        foreach ($arr as $key => $val) {
            $ret[$key] = $val;
        }
        return $ret;
    }
}

if (! function_exists('startsWith')) {
    function startsWith($haystack, $needle) {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }
}

if (! function_exists('endsWith')) {
    function endsWith($haystack, $needle) {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }
}
