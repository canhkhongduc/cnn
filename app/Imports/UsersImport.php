<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;

class UsersImport implements ToModel

{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'name'     => $row[1],
            'student_class'    => $row[2], 
            'generation'    => $row[3], 
            'yob'    => $row[4], 
            'address'    => $row[5], 
            'job'    => $row[6], 
            'company'    => $row[7], 
            'position'    => $row[8], 
            'email'    => $row[9], 
            'phone_number'    => $row[10], 
            'note'    => $row[11], 
            'password' => Hash::make('oncecnnerforevercnner'),
        ]);
    }
}
