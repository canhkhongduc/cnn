<?php

namespace App\Http\Controllers;

use App\Models\Title;
use App\Models\Budget;
use App\Models\TitleBudget;
use App\Models\PlanHistory;

class BaseController extends Controller
{
	public $viewdata = array();
	public $session;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->viewdata['page_title'] = "";
		$this->viewdata['active_menu_key'] = "";
	}
	
    public function getSearchConditions($data, $keys, $sessionPrefix) {
    	$conds = array();
    	foreach ($keys as $key) {
    		$conds[$key] = $this->getSessionData($data, $key, $sessionPrefix);
    	}
    	return $conds;
    }

    public function getSessionData($data, $key, $sessionPrefix, $df=null) {
    	$sessionKey = "tagdm." . $sessionPrefix . "." . $key;
    	if (isset($data[$key]) || key_exists('submitted', $data)) {
    		$value = isset($data[$key]) ? $data[$key] : null;
    		$this->session->put(array($sessionKey => $value));
    	}
    	$ret = $this->session->get($sessionKey);
    	if (empty($ret) && !empty($df)) {
    		$ret = $df;
    		$this->session->put(array($sessionKey => $ret));
    	}
    	return $ret;
    }

    public function setSessionData($key, $value, $sessionPrefix, $df=null) {
    	$sessionKey = "tagdm." . $sessionPrefix . "." . $key;
		$value = !empty($value) ? $value : $df;
		\Log::debug($sessionKey.'='.$value);
		$this->session->put(array($sessionKey => $value));
    }

    public function removeSessionData($key, $sessionPrefix) {
    	$sessionKey = "tagdm." . $sessionPrefix . "." . $key;
		$this->session->forget($sessionKey);
    }

	public function likeValue($value, $match_method) {
		switch ($match_method) {
			case MATCH_METHOD_FORWARD:
				return $value . '%';
			case MATCH_METHOD_PARTIAL:
				return '%' . $value . '%';
			case MATCH_METHOD_BACKWARD:
				return '%' . $value;
			default:
				return $value;
		}
	}

	function array_ptr($arr, $keys) {
		if (!is_array($keys)) $keys = array($keys);
		$ret = $arr;
		foreach ($keys as $key) {
			if (!key_exists($key, $ret)) return null;
			$ret = $ret[$key];
		}
		return $ret;
	}

	function array_add($arr1, $arr2) {
		$ret = $arr1;
		foreach ($arr2 as $obj) {
			$ret[] = $obj;
		}
		return $ret;
	}

	function array_distinct($arr, $keys) {
		if (!is_array($arr)) return array();
		$ret = array();
		foreach ($arr as $elm) {
			$val = array_ptr($elm, $keys);
			if (in_array($val, $ret)) continue;
			$ret[] = $val;
		}
		return $ret;
	}

	function array_index($arr, $keys) {
		if (!is_array($arr)) return array();
		if (!is_array($keys)) $keys = array($keys);
		$ret = array();
		foreach ($arr as $e) {
			$keyVals = array();
			foreach ($keys as $key) {
				$keyVals[] = $e[$key];
			}
			$key = implode('-', $keyVals);
			$ret[$key] = $e;
		}
		return $ret;
	}

	function isEmpty($val) {
		if (!empty($val)) return false;
		if ($val === 0) return false;
		if ($val === '0') return false;
		if (is_string($val) && strlen($val) > 0) return false;
		return true;
	}

	function df($str) {
		if (isEmpty($str)) return "";
		return $str;
	}

	function df_date($format, $time) {
		if (empty($time)) return "";
		return date($format, strtotime($time));
	}

	function startsWith($haystack, $needle) {
		return $needle === "" || strpos($haystack, $needle) === 0;
	}

	function endsWith($haystack, $needle) {
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}

	function datediff($a, $b) {
		$datetime_a = new DateTime($a);
		$datetime_b = new DateTime($b);
		$interval = $datetime_b->diff($datetime_a);
		return $interval->format('%r%a');
	}

	function dateadd($date, $interval) {
		$ret = date_add(date_create($date), date_interval_create_from_date_string($interval . ' days'));
		return date_format($ret, 'Y-m-d');
	}

	public function makeDate($conddata, $prefix) {
		$year = empty($conddata[$prefix."_year"]) ? date('Y') : $conddata[$prefix."_year"];
		$month = empty($conddata[$prefix."_month"]) ? 1 : $conddata[$prefix."_month"];
		return date('Y-m-d', mktime(0, 0, 0, $month, 1, $year));
	}
}
