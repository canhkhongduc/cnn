<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UsersImport;
use App\User;

class ImportController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('import');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import_users(Request $request)
    {
        $rows = (new UsersImport)->toArray(request()->file('users_header_file'))[0];
        foreach($rows as $row) {
            $data = array(
                'name' => $row[1],
                'student_class' => $row[2],
                'generation' => $row[3],
                'yob' => $row[4],
                'address' => $row[5],
                'job' => $row[6],
                'company' => $row[7],
                'position' => $row[8],
                'email' => $row[9],
                'phone_number' => $row[10],
                'note' => $row[11],
                'password' => \Hash::make('oncecnnerforevercnner'),
            );
            $id = User::insertGetId($data);
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function import_users_2(Request $request)
    {
        $rows = (new UsersImport)->toArray(request()->file('users_header_file'))[0];
        foreach($rows as $row) {
            $data = array(
                'name' => $row[0],
                'phone_number' => $row[1],
                'facebook' => $row[2],
                'experience' => $row[3],
                'student_class' => $row[4],
                'generation' => $row[5],
                'yob' => $row[6],
                'address' => $row[7],
                'job' => $row[8],
                'company' => $row[9],
                'position' => $row[10],
                'website' => $row[11],
                'email' => $row[14],
                'note' => $row[13],
                'password' => \Hash::make('oncecnnerforevercnner'),
            );
            $user = User::where('email', $row[14])->get()->first();
            if(!empty($user)) {
                User::where('email', $row[14])->update($data);
            } else {
                $id = User::insertGetId($data);
            }
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
