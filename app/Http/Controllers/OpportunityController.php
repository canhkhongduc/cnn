<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UsersImport;
use App\User;
use App\Models\Opportunity;
use App\Models\OpportunityUser;
use Illuminate\Support\Facades\Validator;

class OpportunityController extends BaseController
{
    /**
     * Display a listing of the opportunities.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post = $request->all();
        $this->session = $request->session();
        $email = $request->input('email');
        $keys = array("search_word", "date_from", "date_to");
        
        $conddata = $this->getSearchConditions($post, $keys, "opportunity.index");
        if(!empty($email)) {
            $user = User::where('email', $email)->get()->first();
            $opportunities = Opportunity::where('create_user_id', $user['id'])->orderBy('date_from', 'desc')->get();
        } else if(empty($conddata['search_word']) && empty($conddata['date_from']) && empty($conddata['date_to'])){
            $opportunities = Opportunity::where('deleted', null)->orderBy('date_from', 'desc')->get();
        } else {
            $query = Opportunity::query();
            $query->distinct()->select('opportunities.id', 'opportunities.create_user_id', 'opportunities.title', 'opportunities.content', 'opportunities.date_from', 'opportunities.date_to', 'opportunities.status');
            $query->leftJoin('users', 'opportunities.create_user_id', '=', 'users.id');
            $query->where('opportunities.deleted', null);
            if (!empty($conddata['search_word'])) {
                $value = $this->likeValue($conddata["search_word"], 'partial');
                $query->where(function($query) use ($value) {
                    $query->orWhere('opportunities.title', 'LIKE', $value);
                    $query->orWhere('opportunities.content', 'LIKE', $value);
                    $query->orWhere('users.name', 'LIKE', $value);
                });
            }

            if (!empty($conddata['main_assignee_user_id'])) {
                $query->where("titles.main_assignee_user_id", intval($conddata['main_assignee_user_id']));
            }
            if (!empty($conddata['date_from'])) {
                $query->where('date_from', '>=', $conddata['date_from']);
            }
            if (!empty($conddata['date_to'])) {
                $query->where('date_to', '<=', $conddata['date_to']);
            }

            $query->orderBy('opportunities.date_from', 'desc');
            $opportunities = $query->get();
        }
        foreach($opportunities as $opportunity) {
            $create_user = User::find($opportunity['create_user_id']);
            $interested_users = OpportunityUser::where('opportunity_id', $opportunity['id'])->where('deleted', null)->get()->toArray();
            foreach($interested_users as $user) {
                if($user['interested_user_id'] == \Auth::user()->id) {
                    $opportunity['interested'] = true;
                }
            }
            $opportunity['create_user'] = $create_user;
            $opportunity['interested_users'] = $interested_users;
        }
        $this->viewdata['current_user_id'] = \Auth::user()->id;
        $this->viewdata['opportunities'] = $opportunities;
        $this->viewdata = array_merge($this->viewdata, $conddata);
        return view('opportunities/index', $this->viewdata);
    }

    /**
     * Display opportunity create form.
     *
     */
    public function showCreateForm() {
        return view('opportunities/create');
    }

    /**
     * Display opportunity create form.
     *
     */
    public function showUpdateForm(Request $request) {
        $id = $request->input('id');
        $opportunity = Opportunity::find($id);

        if(empty(\Auth::user()->id) || $opportunity['create_user_id'] != \Auth::user()->id) {
            abort(403, 'Unauthorized action.');
        }
        $this->viewdata['opportunity'] = $opportunity;
        return view('opportunities/update', $this->viewdata);
    }


    /**
     * Save opportunity
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $post = $request->all();
        $validator = Validator::make($post,
            [
                'title'       => 'required',
                'date_from'      => 'required|date',
                'content' => 'required'
            ], [
            ], [
                'title'       => 'tiêu đề',
                'date_from'      => 'ngày bắt đầu',
                'content' => 'nội dung'
            ]
        );
        if(count($validator->errors()) > 0) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            
            $data = array(
                'title' => $post['title'],
                'date_from' => $post['date_from'],
                'date_to' => $post['date_to'],
                'content' => $post['content'],
                'create_user_id' => \Auth::user()->id
            );
            $saved = Opportunity::insert($data);
           if($saved) {
                $alert = 'success';
                $message = 'Đăng dự án thành công.';
            } else {
                $alert = 'error';
                $message = 'Đăng dự án thất bại.';
            }
            return redirect('opportunities')->with($alert, $message);
        }
        
    }

    /**
     * Update a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $post = $request->all();
        $opportunity = Opportunity::find($post['id']);
        if($opportunity['create_user_id'] != \Auth::user()->id) {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($post,
            [
                'title'       => 'required',
                'date_from'      => 'required|date',
                'content' => 'required'
            ], [
            ], [
                'title'       => 'tiêu đề',
                'date_from'      => 'ngày bắt đầu',
                'content' => 'nội dung'
            ]
        );
        if(count($validator->errors()) > 0) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            
            $data = array(
                'title' => $post['title'],
                'date_from' => $post['date_from'],
                'date_to' => $post['date_to'],
                'content' => $post['content']
            );
            $saved = $opportunity->update($data);
           if($saved) {
                $alert = 'success';
                $message = 'Cập nhật dự án thành công.';
            } else {
                $alert = 'error';
                $message = 'Cập nhật án thất bại.';
            }
            return redirect('opportunities')->with($alert, $message);
        }
    }

    /**
     * Update a user interested in an opportunity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function interest(Request $request)
    {
        $post = $request->all();
        $post['status'] = 1;
        $saved = OpportunityUser::insert($post);
        if($saved) {
            return \Response::json($post);
        }
    }

    /**
     * Update a user interested in an opportunity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function disinterest(Request $request)
    {
        $post = $request->all();
        $deleted = OpportunityUser::where('interested_user_id', $post['interested_user_id'])->where('opportunity_id', $post['opportunity_id'])->get()->first()->delete();
        if($deleted) {
            return \Response::json($post);
        }
    }

    /**
     * Update a user interested in an opportunity.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function interested()
    {
        $opportunity_users = OpportunityUser::where('interested_user_id', \Auth::user()->id)->where('deleted', null)->with('opportunity')->get()->toArray();
        $opportunities = array();
        $conddata = array(
            'search_word' => null,
            'date_from' => null,
            'date_to' => null
        );
        foreach($opportunity_users as $opportunity_user) {
            $opportunity = Opportunity::where('id', $opportunity_user['opportunity_id'])->where('deleted', null)->get()->first();
            $opportunity['create_user'] = User::find($opportunity['create_user_id']);
            $opportunity['interested_users'] = $opportunity_user;
            $opportunity['interested'] = true;
            array_push($opportunities, $opportunity);
        }
        $this->viewdata['current_user_id'] = \Auth::user()->id;
        $this->viewdata['opportunities'] = $opportunities;
        $this->viewdata = array_merge($this->viewdata, $conddata);
        return view('opportunities/index', $this->viewdata);
    }

    /**
     * Delete the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $post = $request->all();
        $id = $post['id'];
        // dd(Opportunity::find($id));
        $deleted = Opportunity::find($id)->update(['deleted' => date('Y-m-d H:i:s')]);
        if($deleted) {
            $alert = 'success';
            $message = 'Xóa dự án thành công.';
        } else {
            $alert = 'success';
            $message = 'Xóa dự án thất bại.';
        }
        return redirect('opportunities')->with($alert, $message);
    }

    /**
     * View list of interested users
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function listInterested(Request $request)
    {
        $post = $request->all();
        $opportunity_id = $post['opportunity_id'];
        $opportunity_users = OpportunityUser::where('opportunity_id', $opportunity_id)->get();
        foreach($opportunity_users as $opportunity_user) {
            $user = User::find($opportunity_user['interested_user_id']);
            $opportunity_user['user'] = $user;
        }   
        return \Response::json($opportunity_users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
