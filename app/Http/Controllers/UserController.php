<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', $this->viewdata);
    }
    public function changePassword(Request $request)
    {
        $this->viewdata = array();
        return view('users/changePassword', $this->viewdata);
    }
    public function updatePassword(Request $request)
    {
        $user = \Auth::user();
        $post = $request->all();
        if( Hash::check($post['current_password'], $user->password) ) {
            $data = array(
                'password' =>  Hash::make($post['password'])
            );
            $saved = User::where('id', $user->id)->update($data);
            if($saved) {
                $alert = 'success';
                $message = 'Cập nhật mật khẩu thành công.';
            } else {
                $alert = 'error';
                $message = 'Cập nhật mật khẩu thất bại.';
            }
        } else {
            $alert = 'error';
            $message = 'Mật khẩu hiện tại không đúng.';
        }
        
        return redirect('users/changePassword')->with($alert, $message);
    }

    public function profile(Request $request) {
        $email = $request->input('email');
        if(!empty($email)) {
            $this->viewdata['user'] = User::where('email', $email)->get()->first();
            $this->viewdata['editable'] = false;
        } else {
            $this->viewdata['user'] = \Auth::user();
            $this->viewdata['editable'] = true;
        }
        return view('users/profile', $this->viewdata);
    }

    public function update(Request $request) {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255']
        ], [], []);
        $post = $request->all();
        $data = array(
            'name' => $post['name'],
            'email' => $post['email'],
            'student_class'=> $post['student_class'],
            'generation' => $post['generation'],
            'yob' => $post['yob'],
            'address' => $post['address'],
            'job' => $post['job'],
            'website' => $post['website'],
            'position' => $post['position'],
            'company' => $post['company'],
            'phone_number' => $post['phone_number'],
            'facebook' => $post['facebook'],
            'experience' => $post['experience'],
            'note' => $post['note']
        );
        $id = \Auth::user()->id;
        $saved = User::where('id', $id)->update($data);
        if($saved){
            $alert = 'success';
            $message = 'Cập nhật thông tin thành công.';
        } else {
            $alert = 'error';
            $message = 'Cập nhật thông tin thất bại.';
        }
        return redirect('users/profile')->with($alert, $message);
    }
}
